Module supplying most of the things for SSO in CERN

basically follows:
http://linux.web.cern.ch/linux/scientific6/docs/shibboleth.shtml

Expects two parameters from hiera:

sso_application_name: 
 - string to identify you application in the CERN SSO
 - is compulsory. It needs to be put to /etc/shibboleth/shibboleth2.xml 

sso_site_name:
 - domain name alias for the application
 - if not set, fqdn fact is used

sso_allowed_groups:
 - space separated list of allowed egroups
 - if not set, "computing-groups" egroup is used

shibboleth_memcached_enabled:
 - enable Memcached support. The RPM MUST be compiled with Memcached support
 - defaults to false

shibboleth_memcached_prefix:
 - prefix for the cache keys
 - defauls to "SHIB:"

shibboleth_memcached_servers:
 - list of memcached servers (host:port).
 - defaults to ["mc.example.org"]

If you want to include the SSO fragment in /etc/myfile, create a class in your
module, something like

class yourmodule::sso {
    include concat::setup
    include shibboleth

    concat{/etc/myfile: }

    concat::fragment{'sso_header':
        target => "/etc/myfile",
        order  => 0,
        content => "header\n",
    }
    concat::fragment{'sso_body':
        target => "/etc/myfile",
        order  => 1,
        content => template('shibboleth/rules.erb'),
    }
    concat::fragment{'sso_footer':
        target => "/etc/myfile",
        order  => 2,
        content => "footer\n",
    }
}

Don't forget that you still must go to
http://www.cern.ch/winservices/SSO/registerapplication.aspx
and register your service with application name and application URI

Application Name: sso_application_name
Application Uri: sso_site_name or $fqdn if not set
