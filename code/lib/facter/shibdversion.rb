# Fact: shibdversion
#
# Purpose: Report the version of cvmfs
#
Facter.add(:shibdversion) do
  confine :kernel => "Linux"
  setcode do
    begin
      Facter::Util::Resolution.exec('/usr/sbin/shibd -v 2>&1').split(' ')[1]
    rescue Exception
      Facter.debug('shibd not available')
    end
  end
end

