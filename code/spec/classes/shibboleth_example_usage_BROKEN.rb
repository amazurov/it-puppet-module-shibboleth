require 'spec_helper'

describe 'shibboleth::example_usage' do
    it do
        Puppet::Util::Log.level = :debug
        Puppet::Util::Log.newdestination(:console)
        should contain_file('/tmp/sso_fragment_example')\
            .with_content(/^Require ADFS_GROUP test_sso_allowed_groups$/)
    end

end


