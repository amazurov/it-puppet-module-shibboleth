class shibboleth::install {


   $installed_pkgs = ['shibboleth', 'liblog4shib1', 'xmltooling-schemas',
                       'opensaml-schemas','shibboleth-selinux']

   package {$installed_pkgs:
       ensure => 'present',
   }

   $_shibmod = $::operatingsystemmajrelease ? {
     '6'    => '/usr/lib64/shibboleth/mod_shib_22.so',
    default => '/usr/lib64/shibboleth/mod_shib_24.so',
   }

   file{'/usr/lib64/httpd/modules/mod_shib2.so':
     ensure  => link,
     target  => $_shibmod,
     before  => Service['httpd'],
     require => [Package['shibboleth'],Package['httpd']]
   }


#    $base_url = 'http://linux.web.cern.ch/linux/scientific6/docs/shibboleth'
#    $base_localpath = '/etc/shibboleth'
#
#    define download_sso_file {
#        $fw = "${shibboleth::install::base_url}/${title}"
#        $fl = "${shibboleth::install::base_localpath}/${title}"
#
#        exec { "backup $title":
#            command => "/bin/mv ${fl} ${fl}.old",
#            creates => "${fl}.old",
#        }
#
#        exec { "download $title":
#            command     => "wget -O ${fl} ${fw}",
#            subscribe   => Exec["backup $title"],
#            refreshonly => true,
#        }
#
#    }
#
#    $cern_sso_resources = [
#        "shibboleth2.xml", "ADFS-metadata.xml", "attribute-map.xml",
#        "wsignout.gif"
#    ]
#
#    download_sso_file { $cern_sso_resources:
#        require => Package['shibboleth'],
#    }
#
#    exec { "process shibboleth2.xml":
#        command => "sed -i 's/somehost/${hostname}/g' ${base_localpath}/shibboleth2.xml",
#        refreshonly => true,
#        subscribe   => Exec["download shibboleth2.xml"]
#    }

}
