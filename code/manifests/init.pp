class shibboleth (
  $allowed_groups      = hiera('sso_allowed_groups', 'computing-groups'),
  $application_name    = hiera('sso_application_name'),
  $attribute_prefix    = hiera('attribute_prefix', ''),
  $site_name           = hiera('sso_site_name',$::fqdn),
  $remote_user         = hiera('sso_remote_user','user'),
  $support_contact     = hiera('sso_support_contact','somebody@cern.ch'),
  $relay_state         = hiera('sso_relay_state','cookie'),
  $remote_user_enabled = hiera('shibboleth_remote_user_enabled', true),
  $memcached_enabled   = hiera('shibboleth_memcached_enabled', false),
  $memcached_prefix    = hiera('shibboleth_memcached_prefix', 'SHIB:'),
  $memcached_servers   = join(hiera('shibboleth_memcached_servers', ['mc.example.org']), ",")
) inherits shibboleth::params {


    anchor{'shibboleth::begin':} ->
    class {'shibboleth::install':} ->
    class {'shibboleth::config':} ~>
    class {'shibboleth::service':} ->
    anchor{'shibboleth::end':}
}   

