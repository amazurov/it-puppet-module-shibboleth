class shibboleth::config {
  if $::operatingsystemmajrelease <= 6 {
      file { '/etc/sysconfig/shibd':
        source => 'puppet:///modules/shibboleth/sysconfig/shibd',
        mode   => '0644',
        require => Package['shibboleth'],
        notify => Service['shibd']
      }
  }

  # Defaults for the majority of files.
  File{
     ensure => file,
     mode   => '0644',
     require => Package['shibboleth'],
     notify  => Service['shibd'],
     owner   => root,
     group   => root
  }

  file{'/etc/shibboleth/wsignout.gif':
    source  => 'puppet:///modules/shibboleth/wsignout.gif',
  }
  file{'/etc/shibboleth/attribute-map.xml':
    source  => 'puppet:///modules/shibboleth/attribute-map.xml',
  }

  # This file is special in that is also loaded periodically from
  # a webserver with a running shibd.
  file{'/etc/shibboleth/ADFS-metadata.xml':
    source                  => 'puppet:///modules/shibboleth/ADFS-metadata.xml',
    selinux_ignore_defaults => true,
    replace                 => false
  }

  file {'/etc/shibboleth/shibboleth2.xml':
     content => template('shibboleth/shibboleth2.xml.erb'),
  }

#
# AUGEAS JOB
#
$shibboleth = '/etc/shibboleth/shibboleth2.xml.copy'
$appName='testo'
$url="$appName.its.cern.ch"

#augeas{'shibboleth_xml':
#	lens    => "Xml.lns",
#        incl    => $shibboleth,
#	context => "/files${shibboleth}",
#	changes => [
	# 1
#	  "defnode Host SPConfig/RequestMapper/RequestMap/Host[./#attribute/applicationId = $appName ] #empty",
#	  "set $Host/#attribute/applicationId $appName",
#	  "set $Host/#attribute/name $url",
	# 2
#	  "set SPConfig/ApplicationDefaults/saml:Audience[#text = https://$url/Shibboleth.sso/ADFS]/#text https://$url/Shibboleth.sso/ADFS",
#	],
#}

# end of AUGEAS
}


