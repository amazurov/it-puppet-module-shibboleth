class shibboleth::service {
    service { "shibd":
               ensure     => running,
               hasstatus  => true,
               hasrestart => true,
               enable     => true,
    }
}

